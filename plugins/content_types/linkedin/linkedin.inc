<?php

require_once 'simple_html_dom.php';
/**
 *  LinkedIn panels plugin.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t("LinkedIn"),
  'description' => t('Get public user profile from site LinkedIn.'),
  'edit form' => 'linkedin_pane_content_type_edit_form',
  'render callback' => 'linkedin_pane_content_type_render',
  'category' => 'LinkedIn',
  'defaults' => array(
    'url' => ''
  ),
);

/**
 * Plugin settings form.
 */
function linkedin_pane_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('LinkId URL'),
    '#description' => t('Enter LinkId public URL to profile user'),
    '#default_value' => $conf['url'],
  );
   $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Select type view'),
    '#options' => array(
      0 => t('Small profile'),
      1 => t('Full profile'),
    ),
    '#default_value' => $conf['type'],
    '#description' => t('Select type LinkedId profile for view.'),
  );
  return $form;
}

/**
 *  Save form data. 
 */
function linkedin_pane_content_type_edit_form_submit($form, &$form_state) {
  $form_state['conf']['url'] = $form_state['values']['url'];
  $form_state['conf']['type'] = $form_state['values']['type'];
}

/**
 *  Render callback function. 
 */
function linkedin_pane_content_type_render($subtype, $conf, $args, $context) {
  if (!empty($conf['url'])) {
    $content = linkedin_get_user_profile($conf['url'], $conf['type']);
  }
  else {
    $content = 'URL not found.';
  }
  $block = new stdClass();
  $block->title = t('LinkedIn user profile');
  $block->content = $content;

  return $block;
}

/**
 *  Get LinkedId user profile from URL.
 */
function linkedin_get_user_profile($url, $type) {
// Full prifile id="content". Small profile class="profile-header".
  $data = file_get_html($url);
  // View all profile data.
  if ($type == 1) {
    if (count($data->find('#content'))) {
      foreach ($data->find('#content') as $div) {
        $content = $div->innertext;
      }
    }
  }
  else {
    if (count($data->find('div.profile-header'))) {
      foreach ($data->find('div.profile-header') as $div) {
        $content = $div->innertext;
      }
    }
  }
  return $content;
}

