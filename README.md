﻿Module LinkedIn ver 1.0
Developer Danilevsky Kirill k.danilevsky@gmail.com

This module allows to get a user profile from the site LinkedIn. The module works with Ctools and Panels modules. 
The module creates a new plugin for Panels, which will appear in adding content. 
It takes the form of options to introduce a link to user profile on linkedin.com website and select the output profile list way (short or full). 
A feature of this module is that it is not required to be authenticated or take any confirmation. 
If the profile is available, the data will be received without a problem.